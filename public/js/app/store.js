
let score,
  settings,
  resources,
  secondsPerBeat,
  secondsPerPulse,
  secondsPerMeasure,
  scoreIndex = 0;

export function setupStore(dataFunction, scoreFunction) {
  ({score, settings, resources} = dataFunction());

  setTiming();
  score = scoreFunction(musicToTime);
  score = convertScoreToMilliseconds();
  score = addResourceDataToClips();
  settings.endTime = getEndTime();
}

export function getSettings() {
  return settings;
}

function setTiming () {
  const { bpm, ppqn, timesignature, } = settings;
  const { numerator, denominator, } = timesignature;

  const pulsesPerBeat = ppqn * (4 / denominator),
      pulsesPerMeasure = pulsesPerBeat * denominator;

  secondsPerBeat = 60 / bpm,
  secondsPerPulse = secondsPerBeat / pulsesPerBeat,
  secondsPerMeasure = pulsesPerMeasure * secondsPerPulse;
}

function musicToTime(timestamp) {
  if (typeof timestamp === 'string') {
    const timeArray = timestamp.split(':');
    return (parseInt(timeArray[0]) * secondsPerMeasure) +
      (parseInt(timeArray[1]) * secondsPerBeat) +
      (parseInt(timeArray[2]) * secondsPerPulse);
  } else if(typeof timestamp === 'number') {
    return timestamp;
  }
  return 0;
}

/**
 * Convert seconds to milliseconds.
 */
function convertScoreToMilliseconds() {
  return score.map(item => {
    if (item.start) {
      item.start *= 1000;
    }
    if (item.end) {
      item.end *= 1000;
    }
    return item;
  });
}

/**
 * Add video resource data to video clip data.
 */
function addResourceDataToClips() {
  return score.reduce((accumulator, item, index) => {
    if (item.resourceID) {
      item.resource = resources.find(resource => resource.id === item.resourceID);
    }
    accumulator.push(item);
    return accumulator;
  }, []);
}

function getEndTime() {
  let endTime = 0;
  score.forEach(item => {
    if (item.end) {
      endTime = Math.max(endTime, item.end);
    }
  });
  return endTime;
}