import { getSettings } from './store.js';

let renderer,
  scene,
  camera,
  msPerFrame;

export function setup(rootEl) {
  const { 
    AmbientLight,
    DirectionalLight,
    OrbitControls,
    PerspectiveCamera,
    Plane,
    Scene,
    TransformControls,
    Vector3,
    WebGLRenderer } = THREE;
  const { canvasWidth, canvasHeight, framerate } = getSettings();
  
  msPerFrame = 1000 / framerate;

  renderer = new WebGLRenderer({antialias: true});
  renderer.setClearColor(0xeeeeee);
  renderer.setSize(canvasWidth, canvasHeight);

  rootEl.appendChild(renderer.domElement);

  scene = new Scene();

  camera = new PerspectiveCamera(45, 1, 1, 500);
  scene.add(camera);

  const ambientLight = new AmbientLight(0xffffff);
  scene.add(ambientLight);

  const directionalLight = new DirectionalLight(0xffffff, 0.5);
  directionalLight.position.set(-0.5, 0.5, -1.5).normalize();
  scene.add(directionalLight);

  const orbit = new OrbitControls( camera, renderer.domElement );
  orbit.update();

  const control = new TransformControls(camera, renderer.domElement);

  draw();
}

/**
 * @see https://stackoverflow.com/questions/11285065/limiting-framerate-in-three-js-to-increase-performance-requestanimationframe
 */
function draw() {
  renderer.render(scene, camera);
  
  setTimeout(() => {
    requestAnimationFrame(draw);
  }, msPerFrame);
}
