import { createScore, getData, } from './data/data.js';
import { setup as setupTimer, start as startTimer, } from './app/timer.js';
import { setup as setupWorld, } from './app/world.js';
import { setupStore, } from './app/store.js';

const worldEl = document.querySelector('#canvas-container');

// init store with data
setupStore(getData, createScore);
setupWorld(worldEl);
setupTimer();
startTimer();
