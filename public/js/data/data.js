
const data = {
  score: [],
  settings: {
    bpm: 120,
    canvasWidth: 640,
    canvasHeight: 480,
    framerate: 30,
    ppqn: 24,
    startOffset: 0,
    throttle: 1,
    timing: 'music',
    timesignature: {
      numerator: 4, // number of beats in a measure
      denominator: 4, // length of a beat (4 = quarter note, 8 = eight note)
    }
  },
  resources: [{
      id: 'main',
      url: '../../frames/main/frame_',
      frames: 16122,
    },
  ],
}

export function createScore(musicToTime) {
  let score = [];

  score = [...score,
    {
      type: 'box',
      w: 10, h: 10, d: 10,
      keyframes: [
        {
          time: 0,
          x: 0, y: 0, z: 0,
        },
        {
          time: 10,
          x: 100, y: 0, z: 0,
        },
      ],
      resourceID: 'main',
    },
  ];

  return score;
}

export function getData() {
  return data;
}
